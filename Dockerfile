FROM python:3.8-slim

WORKDIR /app

COPY sum.py .

CMD ["python", "sum.py"]
