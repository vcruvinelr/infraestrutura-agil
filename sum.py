def sum(a, b):
    return a + b

if __name__ == "__main__":
    a = 3
    b = 5
    print(f"The sum of {a} and {b} is {sum(a, b)}")
